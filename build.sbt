name := "IntelijSparkDemo"

version := "0.1"

//scalaVersion := "2.13.4"
scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-core_2.12" % "3.1.0",
  "org.apache.spark" % "spark-sql_2.12" % "3.1.0"
)

