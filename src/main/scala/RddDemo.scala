import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD

object RddDemo {
  def main(args: Array[String]): Unit = {
    val master = "local[*]"
    val appName = "RddDemo"
    val conf: SparkConf = new SparkConf().setAppName(appName).setMaster(master)
    val sc = new SparkContext(conf)

    val data: Array[Int] = Array(1, 2, 3, 4, 5)
    val distData: RDD[Int] = sc.parallelize(data)
    val x = distData.reduce((a, b) => a + b)
    println(x)

    val lines = sc.textFile("data/Nhung.csv")
//    val lines = sc.textFile("data/test2")
    val lineLengths = lines.map(s => s.length)
    lineLengths.persist()

    val totalLength = lineLengths.reduce((a, b) => a + b)
    println(totalLength)

  }
}
