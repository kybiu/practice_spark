import org.apache.spark.SparkContext
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

object SparkDemo {
  def main(args: Array[String]): Unit = {
    Logger.getRootLogger.setLevel(Level.INFO)
//    val logFile = "YOUR_SPARK_HOME/README.md" // Should be some file on your system
    val logFile = "test_txt" // Should be some file on your system

    val sc = new SparkContext("local[*]", "SparkDemo")
    val lines = sc.textFile("test_txt")
    val words = lines.flatMap(line => line.split(' '))
    val wordsKVRdd = words.map(x => (x, 1))
    val count = wordsKVRdd.reduceByKey((x, y) => x + y).map(x => (x._2, x._1)).sortByKey(false).map(x => (x._2, x._1)).take(10)
    count.foreach(println)
  }
}

//object SparkDemo {
//  def main(args: Array[String]) {
//    val logFile = "test_txt" // Should be some file on your system
//    val spark = SparkSession.builder.appName("SparkDemo").getOrCreate()
//    val logData = spark.read.textFile(logFile).cache()
//    val numAs = logData.filter(line => line.contains("a")).count()
//    val numBs = logData.filter(line => line.contains("b")).count()
//    println(s"Lines with a: $numAs, Lines with b: $numBs")
//    spark.stop()
//  }
//}
//object SparkDemo extends App{
//  val spark = SparkSession.builder
//    .master("local[*]")
//    .appName("Sample App")
//    .getOrCreate()
//  val data = spark.sparkContext.parallelize(
//    Seq("I like Spark", "Spark is awesome", "My first Spark job is working now and is counting down these words")
//  )
//  val filtered = data.filter(line => line.contains("awesome"))
//  filtered.collect().foreach(print)
//}
