# Spark for beginner
## I. RDD Programing

### 1. Basic <br>
**SparkContext:** tells Spark how to access a cluster.

**SparkConf:** to create a SparkContext, need to build SparkConf contains application information

Only one SparkContext should be active per JVM. You must stop() the active SparkContext before creating a new one.


```
val master = "local[*]"    
val appName = "RddDemo"  
val conf: SparkConf = new SparkConf().setAppName(appName).setMaster(master)
val sc = new SparkContext(conf)
```
The appName parameter is a name for your application to show on the cluster UI<br>
The master is a Spark, Mesos or YARN cluster URL, or a special “local” string to run in local mode.

### 2. Resilient Distributed Datasets (RDDs)

There are 2 ways to create RDDs: **Parallelizing an existing collection** in program or referencing a **dataset in an external storage systems** (HDFS, HBase or any data source offering a Hadoop Input format)
+ **Parallelized collections** created by calling **SparkContext's parallelize** method on an existing collection in program
    ```
    val data: Array[Int] = Array(1, 2, 3, 4, 5)
    val distributedData: RDD[Int] = sc.parallelize(data)
    ```

    Cut the dataset into partitions with parameter **partitions**. Spark will run one task for each partitions of the cluster

    Spark set the number of partitions automatically based on your cluster
    ```
    val distributedData: RDD[Int] = sc.parallelize(data, 10)
    ```
+ **External DataSets**
    
    Research later

**Rdd Operations**
RDDs support 2 types of operations:
+ __Transformation__: create a new dataset from existing one
+ __Action__: Return a value to program after running compuation on the dataset

All __transformation__ in Spark are lazy. They do not compute the results right away. They remember the __transformation__ applied to some dataset.

The __transformation__ are only computed when an __action__ requires a result to be returned to program
## Reference
[RDD Programing Guide](https://spark.apache.org/docs/latest/rdd-programming-guide.html)